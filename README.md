# ModderBaas

ModderBaas is a Rust library to interact with installed [Minetest](https://www.minetest.net/) objects (worlds, games, mods), and a accompanying CLI application to manage Minetest mods on headless installations (such as `minetest-server` on Debian).

Note that ModderBaas is currently still a WIP with an unstable API. Expect changes in both the Rust library and the CLI.

## Building

The build process is done through `cargo`:

```
cargo build --release
```

The binary will reside in `target/release/`.

For easy deployment on a server, consider building a statically linked executable, e.g. by using [emk/rust-musl-builder](https://github.com/emk/rust-musl-builder).

## Documentation

Library documentation can be built through

```
cargo doc -p modderbaas
```

Note that the crate is split into a workspace, running `cargo doc` without `-p` will give you the wrong documentation!

The manpage for the CLI can be built by calling `make`. This requires [asciidoctor](https://asciidoctor.org/) to be installed. It will produce `target/modderbaas.1` (the manpage) and `target/modderbaas.html` (a HTML version of the manpage).

## Usage

See `modderbaas help` or refer to the manpage.

**Caution**: Make sure your Minetest server is stopped before you meddle with the installed worlds/games/mods to prevent possible data corruption!

## Hacking

`modderbaas` welcomes contributions, either as a PR or per email to <modderbaas@kingdread.de>.

* Modifications pertaining to the CLI application or the user interaction should go into the `modderbaas-cli` (binary) crate (repository root).
* Modifications pertaining to the core functionality or that are useful for other applications should go into the `modderbaas` (library) crate (in `./modderbaas`).

Some commands (such as `cargo test`) may require you to specify the crate in which they should be run, e.g.

```
cargo test -p modderbaas
```

## License

    Copyright (C) 2021 Daniel Schadt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
