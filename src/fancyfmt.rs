//! This module provides some methods for "fancily" formatting data.
//!
//! This mainly exists so that we can have a version of [`std::fmt::Display`] that can use the
//! colors provided by [`termcolor`].
use std::io::Write;

use anyhow::Result;
use modderbaas::{contentdb::ContentMeta, World};
use termcolor::{Color, ColorSpec, StandardStream, WriteColor};

/// A trait for objects to output them to a (possibly) colored stream.
pub trait ColoredDisplay {
    /// Output the object to the colored stream.
    fn fmt(&self, output: &mut StandardStream) -> Result<()>;
}

impl<T: ColoredDisplay> ColoredDisplay for &T {
    fn fmt(&self, output: &mut StandardStream) -> Result<()> {
        (*self).fmt(output)
    }
}

impl ColoredDisplay for &str {
    fn fmt(&self, output: &mut StandardStream) -> Result<()> {
        write!(output, "{}", *self)?;
        Ok(())
    }
}

impl ColoredDisplay for String {
    fn fmt(&self, output: &mut StandardStream) -> Result<()> {
        (self as &str).fmt(output)
    }
}

impl ColoredDisplay for World {
    fn fmt(&self, output: &mut StandardStream) -> Result<()> {
        write!(output, "{}", self)?;
        Ok(())
    }
}

impl ColoredDisplay for ContentMeta {
    fn fmt(&self, output: &mut StandardStream) -> Result<()> {
        output.set_color(ColorSpec::new().set_fg(Some(Color::Yellow)))?;
        write!(output, "{}", self.title)?;
        output.reset()?;
        write!(output, " by ")?;
        output.set_color(ColorSpec::new().set_fg(Some(Color::Cyan)))?;
        write!(output, "{}", self.author)?;
        output.reset()?;
        write!(output, " - {}", self.short_description)?;
        Ok(())
    }
}
