all: target/modderbaas.html target/modderbaas.1

target/modderbaas.html: modderbaas.adoc
	asciidoctor -o $@ $<

target/modderbaas.1: modderbaas.adoc
	asciidoctor -b manpage -o $@ $<

.PHONY: all
