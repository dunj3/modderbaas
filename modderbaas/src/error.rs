//! Error definitions for ModderBaas.
//!
//! The type alias [`Result`] can be used, which defaults the error type to [`enum@Error`]. Any function
//! that introduces errors should return a [`Result`] — unless it is clear that a more narrow error
//! will suffice, such as [`crate::util::copy_recursive`].
use std::path::PathBuf;

use thiserror::Error;
use url::Url;

/// The main error type.
#[derive(Error, Debug)]
pub enum Error {
    /// A malformed or otherwise invalid mod ID has been given.
    #[error("invalid mod id '{0}'")]
    InvalidModId(String),
    /// The ContentDB website returned invalid data.
    #[error("the website returned unexpected data")]
    InvalidScrape,
    /// The directory does not contain a valid Minetest mod.
    #[error("'{0}' is not a valid mod directory")]
    InvalidModDir(PathBuf),
    /// The directory does not contain a valid Minetest game.
    #[error("'{0}' is not a valid game directory")]
    InvalidGameDir(PathBuf),
    /// The directory does not contain a valid Minetest world.
    #[error("'{0}' is not a valid world directory")]
    InvalidWorldDir(PathBuf),
    /// The directory does not contain a valid Minetest modpack.
    #[error("'{0}' is not a valid modpack directory")]
    InvalidModpackDir(PathBuf),
    /// The given source string can not be parsed into a [`crate::download::Source`].
    #[error("'{0}' does not represent a valid mod source")]
    InvalidSourceSpec(String),

    /// No mod found in the downloaded archive.
    #[error("the downloaded archive from '{0}' did not contain a mod")]
    NoModInArchive(Url),
    /// No mod found in the repository.
    #[error("the repository at '{0}' did not contain a mod")]
    NoModInRepository(Url),
    /// The given world does not have a game ID set.
    #[error("the world has no game ID set")]
    NoGameSet,
    /// The game with the given ID could not be found.
    #[error("the game with ID '{0}' was not found")]
    GameNotFound(String),
    /// ContentDB returned more than one fitting mod for the query.
    #[error("the mod ID '{0}' does not point to a single mod")]
    AmbiguousModId(String),
    /// The Key-Value store has malformed content.
    #[error("syntax error in the metadata")]
    MalformedKVStore,

    /// Wrapper for HTTP errors.
    #[error("underlying HTTP error")]
    UreqError(#[from] ureq::Error),
    /// Wrapper for generic I/O errors.
    #[error("underlying I/O error")]
    IoError(#[from] std::io::Error),
    /// Wrapper for ZIP errors.
    #[error("ZIP error")]
    ZipError(#[from] zip::result::ZipError),
    /// Wrapper for URL parsing errors.
    #[error("invalid URL")]
    UrlError(#[from] url::ParseError),
    /// Wrapper for Git errors.
    #[error("git error")]
    GitError(#[from] git2::Error),
    /// Wrapper for Unix errors.
    #[cfg(unix)]
    #[error("underlying Unix error")]
    NixError(#[from] nix::errno::Errno),
}

/// A [`Result`] with the error type defaulted to [`enum@Error`].
pub type Result<T, E = Error> = std::result::Result<T, E>;
