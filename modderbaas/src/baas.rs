//! This module contains functions to query & manipulate the global Minetest installation.
use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

use dirs;
use log::debug;

use super::{
    contentdb::ContentDb,
    download::{Downloader, Source},
    error::{Error, Result},
    game::Game,
    minemod::{self, MineMod, ModContainer},
    scan,
    world::World,
};

/// Returns a list of folders in which worlds are expected.
///
/// Note that not all of these folders need to exist.
///
/// This returns the following locations:
///
/// * `$HOME/.minetest/worlds`
/// * `/var/games/minetest-server/.minetest/worlds`
pub fn world_dirs() -> Result<Vec<PathBuf>> {
    let mut paths = vec!["/var/games/minetest-server/.minetest/worlds".into()];
    if let Some(home) = dirs::home_dir() {
        paths.push(home.join(".minetest").join("worlds"))
    }
    Ok(paths)
}

/// Returns a list of folders in which games are expected.
///
/// Note that not all of these folders need to exist.
///
/// This returns the following locations:
///
/// * `$HOME/.minetest/games`
/// * `/var/games/minetest-server/.minetest/games`
/// * `/usr/share/minetest/games`
/// * `/usr/share/games/minetest/games`
pub fn game_dirs() -> Result<Vec<PathBuf>> {
    let mut paths = vec![
        "/var/games/minetest-server/.minetest/games".into(),
        "/usr/share/minetest/games".into(),
        "/usr/share/games/minetest/games".into(),
    ];
    if let Some(home) = dirs::home_dir() {
        paths.push(home.join(".minetest").join("games"))
    }
    Ok(paths)
}

/// Returns a list of folders in which mods are expected.
///
/// Note that not all of these folders need to exist.
///
/// This returns the following locations:
///
/// * `$HOME/.minetest/mods`
/// * `/var/games/minetest-server/.minetest/mods`
/// * `/usr/share/games/minetest/mods`
/// * `/usr/share/minetest/mods`
pub fn mod_dirs() -> Result<Vec<PathBuf>> {
    let mut paths = vec![
        "/var/games/minetest-server/.minetest/mods".into(),
        "/usr/share/games/minetest/mods".into(),
        "/usr/share/minetest/mods".into(),
    ];
    if let Some(home) = dirs::home_dir() {
        paths.push(home.join(".minetest").join("mods"))
    }
    Ok(paths)
}

/// The [`Baas`] provides a way to list all worlds, games and mods on the system and allows access
/// via the [`World`], [`Game`] and [`MineMod`] wrappers.
#[derive(Debug, Default, Clone)]
pub struct Baas {
    world_dirs: Vec<PathBuf>,
    game_dirs: Vec<PathBuf>,
    mod_dirs: Vec<PathBuf>,
}

impl Baas {
    /// Create a [`Baas`] with the standard dirs.
    pub fn with_standard_dirs() -> Result<Baas> {
        Ok(Baas::default()
            .with_world_dirs(world_dirs()?)
            .with_game_dirs(game_dirs()?)
            .with_mod_dirs(mod_dirs()?))
    }

    /// Replace the world dirs with the given list of world dirs.
    pub fn with_world_dirs(self, world_dirs: Vec<PathBuf>) -> Baas {
        Baas { world_dirs, ..self }
    }

    /// The list of directories which are searched for worlds.
    #[inline]
    pub fn world_dirs(&self) -> &[PathBuf] {
        self.world_dirs.as_slice()
    }

    /// Replace the game dirs with the given list of game dirs.
    pub fn with_game_dirs(self, game_dirs: Vec<PathBuf>) -> Baas {
        Baas { game_dirs, ..self }
    }

    /// The list of directories which are searched for games.
    #[inline]
    pub fn game_dirs(&self) -> &[PathBuf] {
        self.game_dirs.as_slice()
    }

    /// Replace the mod dirs with the given list of mod dirs.
    pub fn with_mod_dirs(self, mod_dirs: Vec<PathBuf>) -> Baas {
        Baas { mod_dirs, ..self }
    }

    /// The list of directories which are searched for mods.
    #[inline]
    pub fn mod_dirs(&self) -> &[PathBuf] {
        self.mod_dirs.as_slice()
    }

    /// Returns a vector of all words that were found in the world dirs.
    pub fn worlds(&self) -> Result<Vec<World>> {
        let mut worlds = vec![];
        for dir in self.world_dirs() {
            match scan(&dir, |p| World::open(p)) {
                Ok(w) => worlds.extend(w),
                Err(e) => debug!("Cannot scan {:?}: {}", dir, e),
            }
        }
        Ok(worlds)
    }

    /// Returns a vector of all games that were found in the game dirs.
    pub fn games(&self) -> Result<Vec<Game>> {
        let mut games = vec![];
        for dir in self.game_dirs() {
            match scan(&dir, |p| Game::open(p)) {
                Ok(g) => games.extend(g),
                Err(e) => debug!("Cannot scan {:?}: {}", dir, e),
            }
        }
        Ok(games)
    }

    /// Returns a vector of all mods that were found in the mod dirs.
    ///
    /// Note that modpacks are flattened into mods.
    pub fn mods(&self) -> Result<Vec<MineMod>> {
        let mut mods = vec![];
        for dir in self.mod_dirs() {
            match scan(&dir, |p| minemod::open_mod_or_pack(p)) {
                Ok(m) => {
                    for container in m {
                        mods.extend(container.mods()?);
                    }
                }
                Err(e) => debug!("Cannot scan {:?}: {}", dir, e),
            }
        }
        Ok(mods)
    }

    /// Return a snapshot of the current state.
    ///
    /// A snapshot "freezes" the lists of worlds, mods and games in time. It is useful to avoid
    /// unnecessary I/O when it is known that the state should not have changed. It also allows
    /// fast searching of items by their name.
    pub fn snapshot(&self) -> Result<Snapshot> {
        let worlds = self.worlds()?;
        let games = self.games()?;
        let mods = self.mods()?;

        Ok(Snapshot {
            worlds: worlds
                .into_iter()
                .map(|w| Ok((w.world_name()?, w)))
                .collect::<Result<_>>()?,
            games: games.into_iter().map(|g| (g.technical_name(), g)).collect(),
            mods: mods
                .into_iter()
                .map(|m| Ok((m.mod_id()?, m)))
                .collect::<Result<_>>()?,
        })
    }

    /// Install the list of mods (given as [`Source`]s) into the given world using the given
    /// [`Installer`].
    ///
    /// The sources are downloaded using the given [`Downloader`].
    ///
    /// This method exists to do most of the dependency resolution work, while still allowing
    /// consumers to customize the installation process.
    pub fn install<I: Installer, S: IntoIterator<Item = Source>>(
        &self,
        mut installer: I,
        downloader: &Downloader,
        world: &World,
        sources: S,
    ) -> Result<(), I::Error> {
        let snapshot = self.snapshot()?;
        let mut wanted = sources.into_iter().collect::<Vec<_>>();

        let mut to_install = Vec::<Box<dyn ModContainer>>::new();
        let mut to_enable = Vec::<MineMod>::new();

        let game_id = world.game_id()?;
        let game = snapshot
            .games()
            .get(&game_id)
            .ok_or(Error::GameNotFound(game_id))?;
        let game_mods = game
            .mods()?
            .into_iter()
            .map(|m| m.mod_id())
            .collect::<Result<Vec<_>, _>>()?;

        'modloop: while !wanted.is_empty() {
            let next_mod = wanted.remove(0);

            // Special handling for mods specified by their ID, as those could already exist.
            if let Source::ModId(ref id) = next_mod {
                if let Some(m) = snapshot.mods().get(id) {
                    // We have that one, just enable it and its dependencies!
                    to_enable.push(m.clone());
                    wanted.extend(m.dependencies()?.into_iter().map(Source::ModId));
                    continue;
                } else if game_mods.contains(id) {
                    // This mod is already contained in the game, nothing for us to do
                    continue;
                }

                // Is this a mod that is already queued for installation?
                for mod_or_pack in &to_install {
                    for m in mod_or_pack.mods()? {
                        if &m.name()? == id {
                            continue 'modloop;
                        }
                    }
                }

                // This mod is not available, so we let the installer resolve it
                wanted.push(installer.resolve(id)?);
            } else {
                let downloaded = downloader.download(&next_mod)?;
                for m in downloaded.mods()? {
                    wanted.extend(m.dependencies()?.into_iter().map(Source::ModId));
                }
                to_install.push(downloaded);
            }
        }

        installer.display_changes(&to_install)?;

        for m in to_install {
            let installed = installer.install_mod(&*m)?;
            to_enable.extend(installed.mods()?);
        }

        for m in to_enable {
            installer.enable_mod(world, &m)?;
        }

        Ok(())
    }
}

/// Snapshot of a [`Baas`] scan.
///
/// A snapshot is created through the [`Baas::snapshot`] method and gives a frozen view on the
/// installed objects.
#[derive(Debug, Clone)]
pub struct Snapshot {
    worlds: HashMap<String, World>,
    games: HashMap<String, Game>,
    mods: HashMap<String, MineMod>,
}

impl Snapshot {
    /// Return all worlds that were found.
    ///
    /// The map maps the world's name to the [`World`] object.
    pub fn worlds(&self) -> &HashMap<String, World> {
        &self.worlds
    }

    /// Return all games that were found.
    ///
    /// The map maps the game ID to the [`Game`] object.
    pub fn games(&self) -> &HashMap<String, Game> {
        &self.games
    }

    /// Return the available mods that were found.
    ///
    /// The map maps the mod name to the [`MineMod`] object.
    pub fn mods(&self) -> &HashMap<String, MineMod> {
        &self.mods
    }
}

/// The [`Installer`] trait allows users of this library to customize the installation process.
///
/// The hooks defined here are called by [`Baas::install`] to carry out the requested task.
#[allow(unused_variables)]
pub trait Installer {
    /// The error that this installer may return.
    ///
    /// Any custom error type is acceptable, but we have to be able to return [`Error`]s from it.
    type Error: From<Error>;

    /// Resolve the "short" mod ID to a proper mod.
    ///
    /// Since a [`Source::ModId`] can be ambiguous, the installer will call this function to
    /// resolve those IDs. You can choose how to implement this, either by asking the user or by
    /// using heuristics to select the right one.
    ///
    /// Note that returning another [`Source::ModId`] from this method is legal, but may result in
    /// an endless loop.
    fn resolve(&mut self, mod_id: &str) -> Result<Source, Self::Error>;

    /// Display the list of mods to be installed to the user.
    ///
    /// This is done before any changes are made to the world and can be used to get confirmation.
    ///
    /// If this function returns `Ok(())`, the process will continue. On any error reply, the
    /// installation is cancelled.
    ///
    /// By default, this function simply returns `Ok(())`.
    fn display_changes(&mut self, to_install: &[Box<dyn ModContainer>]) -> Result<(), Self::Error> {
        Ok(())
    }

    /// Install the given mod or modpack, usually by copying the mod files to the appropriate
    /// directory.
    ///
    /// This function is expected to return a mod object pointing to the copied mod.
    ///
    /// The purpose of this function is to customize the copying process, for example if
    /// adjustments have to be made afterwards.
    fn install_mod(
        &mut self,
        mod_or_pack: &dyn ModContainer,
    ) -> Result<Box<dyn ModContainer>, Self::Error>;

    /// Enable the mod in the given world.
    ///
    /// This function per default calls [`World::enable_mod`].
    fn enable_mod(&mut self, world: &World, minemod: &MineMod) -> Result<(), Self::Error> {
        Ok(world.enable_mod(&minemod.mod_id()?)?)
    }
}

/// An [`Installer`] that simply installs mods to the given directory.
///
/// It resolves mods by checking if there is a single candidate. If yes, that one is taken - if
/// not, an error is returned ([`Error::AmbiguousModId`]). No user interaction is expected or done.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DirInstaller<'p> {
    dir: &'p Path,
}

impl<'p> DirInstaller<'p> {
    /// Create a new dir installer that installs mods to the given directory.
    pub fn new(dir: &'p Path) -> Self {
        DirInstaller { dir }
    }

    /// The path to the directory in which this installer installs mods.
    pub fn dir(&self) -> &Path {
        self.dir
    }
}

impl<'p> Installer for DirInstaller<'p> {
    type Error = Error;

    fn resolve(&mut self, mod_id: &str) -> Result<Source, Self::Error> {
        let content_db = ContentDb::new();
        let candidates = content_db.resolve(mod_id)?;
        if candidates.len() == 1 {
            Ok(Source::Http(candidates.into_iter().next().unwrap().url))
        } else {
            Err(Error::AmbiguousModId(mod_id.into()))
        }
    }

    fn install_mod(
        &mut self,
        mod_or_pack: &dyn ModContainer,
    ) -> Result<Box<dyn ModContainer>, Self::Error> {
        mod_or_pack.install_to(self.dir)
    }
}
