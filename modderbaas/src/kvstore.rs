//! Support module for writing `key=value` stores.
//!
//! These files are used by minetest mods (`mod.conf`), games (`game.conf`) and worlds
//! (`world.mt`).
//!
//! Key-Value-Stores (KVStores) are represented by a [`std::collections::HashMap`] on the Rust
//! side.
use std::{collections::HashMap, fs, io::Write, path::Path};

use super::error::{Error, Result};

/// Read the given file as a KVStore.
pub fn read<P: AsRef<Path>>(path: P) -> Result<HashMap<String, String>> {
    let content = fs::read_to_string(path)?;
    parse_kv(&content)
}

/// Parse the given data as a KVStore.
pub fn parse_kv(content: &str) -> Result<HashMap<String, String>> {
    let mut result = HashMap::new();
    let mut current_pair: Option<(String, String)> = None;
    for line in content.lines() {
        if let Some((current_key, mut current_value)) = current_pair.take() {
            if line == r#"""""# {
                result.insert(current_key, current_value.trim().into());
            } else {
                current_value.push_str(line);
                current_value.push('\n');
                current_pair = Some((current_key, current_value));
            }
        } else if line.is_empty() {
            // Skip empty lines outside of long strings
        } else if let [key, value] = line
            .splitn(2, '=')
            .map(str::trim)
            .collect::<Vec<_>>()
            .as_slice()
        {
            let (key, value): (String, String) = (String::from(*key), String::from(*value));
            if value == r#"""""# {
                current_pair = Some((key, String::new()));
            } else {
                result.insert(key, value);
            }
        } else {
            return Err(Error::MalformedKVStore);
        }
    }

    Ok(result)
}

/// Write the given KVStore back to the file.
///
/// The order of the keys is guaranteed to be the following:
///
/// 1. All options that *don't* start with `"load_mod_"` are saved in alphabetical order.
/// 2. All remaining options are saved in alphabetical order.
///
/// Note that this function will **override** existing files!
pub fn save<P: AsRef<Path>>(data: &HashMap<String, String>, path: P) -> Result<()> {
    let output = fs::File::create(path)?;
    write(data, output)
}

/// Write the given KVStore to a [`Write`]r.
///
/// See [`save`] for information about the key ordering.
pub fn write<W: Write>(data: &HashMap<String, String>, mut writer: W) -> Result<()> {
    let mut items = data.iter().collect::<Vec<_>>();
    items.sort_by_key(|i| (if i.0.starts_with("load_mod_") { 1 } else { 0 }, i.0));

    for (key, value) in items {
        if !value.contains('\n') {
            writeln!(writer, "{} = {}", key, value)?;
        } else {
            writeln!(writer, r#"{0} = """{1}{2}{1}""""#, key, '\n', value)?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    use indoc::indoc;

    #[test]
    fn test_read() {
        let text = indoc! {r#"
            name = climate_api
            title = Climate API
            author = TestificateMods
            release = 10001
            optional_depends = player_monoids, playerphysics, pova
            description = """
            A powerful engine for weather presets and visual effects.
            Use the regional climate to set up different effects for different regions.
            Control where your effects are activated based on temperature, humidity, wind,
            position, light level or a completely custom activator.
            Climate API provides temperature and humidity values on a block-per-block basis
            that follow the seasons, day / night cycle and random changes.
            Make it rain, change the sky or poison the player - it's up to you.

            Climate API requires additional weather packs in order to function.
            Try regional_weather for the best experience.
            """
        "#};
        let kvstore = parse_kv(text).unwrap();
        let wanted_store = [
            ("name", "climate_api"),
            ("title", "Climate API"),
            ("author", "TestificateMods"),
            ("release", "10001"),
            ("optional_depends", "player_monoids, playerphysics, pova"),
            (
                "description",
                indoc! {"
                A powerful engine for weather presets and visual effects.
                Use the regional climate to set up different effects for different regions.
                Control where your effects are activated based on temperature, humidity, wind,
                position, light level or a completely custom activator.
                Climate API provides temperature and humidity values on a block-per-block basis
                that follow the seasons, day / night cycle and random changes.
                Make it rain, change the sky or poison the player - it's up to you.

                Climate API requires additional weather packs in order to function.
                Try regional_weather for the best experience.\
                "},
            ),
        ]
        .iter()
        .map(|&(k, v)| (k.into(), v.into()))
        .collect::<HashMap<String, String>>();
        assert_eq!(kvstore, wanted_store);
    }

    #[test]
    fn test_write() {
        let mut output = Vec::new();
        let store = [
            ("name", "climate_api"),
            ("title", "Climate API"),
            ("author", "TestificateMods"),
            ("release", "10001"),
            ("optional_depends", "player_monoids, playerphysics, pova"),
            (
                "description",
                indoc! {"
                A powerful engine for weather presets and visual effects.
                Use the regional climate to set up different effects for different regions.
                Control where your effects are activated based on temperature, humidity, wind,
                position, light level or a completely custom activator.
                Climate API provides temperature and humidity values on a block-per-block basis
                that follow the seasons, day / night cycle and random changes.
                Make it rain, change the sky or poison the player - it's up to you.

                Climate API requires additional weather packs in order to function.
                Try regional_weather for the best experience.\
                "},
            ),
        ]
        .iter()
        .map(|&(k, v)| (k.into(), v.into()))
        .collect::<HashMap<String, String>>();

        write(&store, &mut output).unwrap();

        let output = std::str::from_utf8(&output).unwrap();
        let expected = indoc! {r#"
            author = TestificateMods
            description = """
            A powerful engine for weather presets and visual effects.
            Use the regional climate to set up different effects for different regions.
            Control where your effects are activated based on temperature, humidity, wind,
            position, light level or a completely custom activator.
            Climate API provides temperature and humidity values on a block-per-block basis
            that follow the seasons, day / night cycle and random changes.
            Make it rain, change the sky or poison the player - it's up to you.

            Climate API requires additional weather packs in order to function.
            Try regional_weather for the best experience.
            """
            name = climate_api
            optional_depends = player_monoids, playerphysics, pova
            release = 10001
            title = Climate API
        "#};

        assert_eq!(output, expected);
    }
}
