//! ModderBaas is a library that allows you to manage Minetest mods.
//!
//! The main point of the library is to be used by the `modderbaas` CLI application, however it can
//! also be used in other Rust programs that want a (limited) API to interact with Minetest
//! content.
//!
//! # Representation
//!
//! Most objects ([`Game`], [`World`], [`MineMod`], [`Modpack`]) are represented by the path to the
//! on-disk directory that contains the corresponding game/world/mod/modpack. The initializers take
//! care that the directory contains a valid object.
//!
//! Many of the query operations do not do any caching, so avoid calling them in tight loops. If
//! you want to do a lot of name queries (e.g. to find installed mods), consider using a
//! [`Snapshot`].
//!
//! # Racing
//!
//! ModderBaas expects that during the time of its access, no other application meddles with the
//! directories. It will *not* lead to crashes, but you will get a lot more error return values.
//!
//! # Mutability
//!
//! Some objects support mutating their state. This is usually done by directly writing the data
//! into the corresponding file. Therefore, those methods (even though they mutate state) only take
//! a shared reference (`&self`) — keep that in mind!
//!
//! # Crate Structure
//!
//! Most game objects are implemented in their own module and re-exported at the crate root:
//!
//! * [`game`]
//! * [`minemod`]
//! * [`world`]
//!
//! Interacting with mods from the internet/ContentDB is done through two modules:
//!
//! * [`contentdb`]
//! * [`download`]
//!
//! Some modules contain auxiliary functions:
//!
//! * [`kvstore`]
//! * [`util`]
//!
//! Error definitions are in [`error`].
//!
//! [`baas`] contains functions and data to interact with the global Minetest installation.
use std::{fs, path::Path};

use log::debug;

macro_rules! regex {
    ($re:literal $(,)?) => {{
        static RE: once_cell::sync::OnceCell<regex::Regex> = once_cell::sync::OnceCell::new();
        RE.get_or_init(|| regex::Regex::new($re).unwrap())
    }};
}

pub mod baas;
pub mod contentdb;
pub mod download;
pub mod error;
pub mod game;
pub mod kvstore;
pub mod minemod;
pub mod util;
pub mod world;

pub use baas::{Baas, Snapshot};
pub use contentdb::ContentDb;
pub use download::{Downloader, Source};
pub use error::Error;
pub use game::Game;
pub use minemod::{MineMod, Modpack};
pub use world::World;

use error::Result;

/// Scan all files in the given directory.
///
/// Files for which `scanner` returns `Ok(..)` will be collected and returned. Files for which
/// `scanner` returns `Err(..)` will be silently discarded.
///
/// This function is useful to iterate through the items in a directory and find fitting objects:
///
/// ```no_run
/// use modderbaas::{scan, minemod::MineMod};
/// let mods: Vec<MineMod> = scan("/tmp", |p| MineMod::open(p))?;
/// # Ok::<(), Box<dyn std::error::Error>>(())
/// ```
pub fn scan<W, P: AsRef<Path>, F: for<'p> Fn(&'p Path) -> Result<W>>(
    path: P,
    scanner: F,
) -> Result<Vec<W>> {
    debug!("Scanning through {:?}", path.as_ref());
    let mut good_ones = vec![];
    for entry in fs::read_dir(path)? {
        let entry = entry?;
        if let Ok(i) = scanner(&entry.path()) {
            good_ones.push(i);
        }
    }
    Ok(good_ones)
}
